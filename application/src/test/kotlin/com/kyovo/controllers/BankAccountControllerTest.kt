package com.kyovo.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.kyovo.Routes
import com.kyovo.configurations.DomainConfiguration
import com.kyovo.exception_handlers.ApiErrorResponseBody
import com.kyovo.view_models.AccountBalanceResource
import com.kyovo.view_models.DepositResource
import com.kyovo.view_models.TransactionResource
import com.kyovo.view_models.WithdrawResource
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

@SpringBootTest
@AutoConfigureMockMvc
@Import(DomainConfiguration::class)
class BankAccountControllerTest @Autowired constructor(
        private val mockMvc: MockMvc,
        private val objectMapper: ObjectMapper)
{
    private companion object
    {
        const val DUMMY_USERNAME = "dummy"
        const val DUMMY_PASSWORD = "dummy"

        const val DEPOSIT_ROUTE = "${Routes.ACCOUNT}/deposit"
        const val WITHDRAWAL_ROUTE = "${Routes.ACCOUNT}/withdraw"
        const val TRANSACTIONS_HISTORY_ROUTE = "${Routes.ACCOUNT}/transactions"
    }

    @Nested
    inner class Deposit
    {
        @Test
        fun `deposit forbidden without authentication`()
        {
            mockMvc
                .post(DEPOSIT_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(DepositResource(1.0))
                }
                .andDo { print() }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @ParameterizedTest
        @ValueSource(doubles = [1000.0, 350.0])
        @DirtiesContext
        @WithMockUser(username = DUMMY_USERNAME, password = DUMMY_PASSWORD)
        fun `make a deposit`(amount: Double)
        {
            mockMvc
                .post(DEPOSIT_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(DepositResource(amount))
                }
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                }

            mockMvc
                .get("${Routes.ACCOUNT}/balance")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(AccountBalanceResource(amount)))
                    }
                }
        }

        @ParameterizedTest
        @ValueSource(doubles = [0.005, 0.0])
        @DirtiesContext
        @WithMockUser(username = DUMMY_USERNAME, password = DUMMY_PASSWORD)
        fun `given deposit amount less than 0,01 then error`(amount: Double)
        {
            mockMvc
                .post(DEPOSIT_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(DepositResource(amount))
                }
                .andDo { print() }
                .andExpect {
                    status { isBadRequest() }
                    content {
                        objectMapper.writeValueAsString(
                            ApiErrorResponseBody(
                                400,
                                "Money must be worth at least 0.01"
                            )
                        )
                    }
                }
        }

        @Test
        @DirtiesContext
        @WithMockUser(username = DUMMY_USERNAME, password = DUMMY_PASSWORD)
        fun `deposit amount should be more than 0,01`()
        {
            mockMvc
                .post(DEPOSIT_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(DepositResource(0.01))
                }
                .andDo { print() }
                .andExpect {
                    status { isBadRequest() }
                    content {
                        objectMapper.writeValueAsString(
                            ApiErrorResponseBody(
                                400,
                                "The deposit amount must be more than 0.01"
                            )
                        )
                    }
                }
        }
    }

    @Nested
    inner class Withdrawal
    {
        @Test
        fun `withdrawal forbidden without authentication`()
        {
            mockMvc
                .post(WITHDRAWAL_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(WithdrawResource(1.0))
                }
                .andDo { print() }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @ParameterizedTest
        @ValueSource(doubles = [250.0, 4500.0])
        @DirtiesContext
        @WithMockUser(username = DUMMY_USERNAME, password = DUMMY_PASSWORD)
        fun `make a withdrawal`(withdrawalAmount: Double)
        {
            val initialBalance = 4500.0
            val finalBalance = initialBalance - withdrawalAmount
            mockMvc
                .post(DEPOSIT_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(DepositResource(initialBalance))
                }
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                }

            mockMvc
                .post(WITHDRAWAL_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(WithdrawResource(withdrawalAmount))
                }
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                }

            mockMvc
                .get("${Routes.ACCOUNT}/balance")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(AccountBalanceResource(finalBalance)))
                    }
                }
        }

        @ParameterizedTest
        @ValueSource(doubles = [-784.0, -1.0, 0.0])
        @DirtiesContext
        @WithMockUser(username = DUMMY_USERNAME, password = DUMMY_PASSWORD)
        fun `withdrawal amount should be more than 0`(amount: Double)
        {
            mockMvc
                .post(WITHDRAWAL_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(WithdrawResource(amount))
                }
                .andDo { print() }
                .andExpect {
                    status { isBadRequest() }
                    content {
                        objectMapper.writeValueAsString(
                            ApiErrorResponseBody(
                                400,
                                "Money must be worth at least 0.01"
                            )
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = DUMMY_USERNAME, password = DUMMY_PASSWORD)
        fun `overdraft is not authorized`()
        {
            mockMvc
                .post(WITHDRAWAL_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(WithdrawResource(400.0))
                }
                .andDo { print() }
                .andExpect {
                    status { isBadRequest() }
                    content {
                        objectMapper.writeValueAsString(
                            ApiErrorResponseBody(
                                400,
                                "Insufficient balance for withdrawal"
                            )
                        )
                    }
                }
        }
    }

    @Nested
    inner class TransactionsHistory
    {
        @Test
        fun `transactions history access forbidden without authentication`()
        {
            mockMvc
                .get("${Routes.ACCOUNT}/transactions")
                .andDo { print() }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @DirtiesContext
        @WithMockUser(username = DUMMY_USERNAME, password = DUMMY_PASSWORD)
        fun `get transactions history`()
        {
            mockMvc
                .post(DEPOSIT_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(DepositResource(450.0))
                }
                .andDo { print() }

            mockMvc
                .post(WITHDRAWAL_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(WithdrawResource(400.0))
                }
                .andDo { print() }

            mockMvc
                .post(DEPOSIT_ROUTE) {
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(DepositResource(500.75))
                }
                .andDo { print() }

            val transactions = listOf(
                TransactionResource(500.75, 550.75),
                TransactionResource(-400.0, 50.0),
                TransactionResource(450.0, 450.0)
            )

            mockMvc
                .get(TRANSACTIONS_HISTORY_ROUTE)
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(transactions))
                    }
                }
        }
    }
}