package com.kyovo

const val API_VERSION = "1.0"
private const val vAPI_VERSION = "v$API_VERSION"

object Routes
{
    const val ERROR = "/error"
    const val API_DOC = "/api/doc"
    const val SWAGGER_UI = "/api/swagger-ui/**"
    const val SWAGGER_V3_API_DOCS = "/v3/api-docs/**"

    const val AUTH = "/api/$vAPI_VERSION/auth"
    const val ACCOUNT = "/api/$vAPI_VERSION/account"
}

object ApiResponseCode
{
    const val OK = "200"
    const val BAD_REQUEST = "400"
    const val FORBIDDEN = "403"
}