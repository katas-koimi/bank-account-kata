package com.kyovo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

//@SpringBootApplication(exclude = [SecurityAutoConfiguration::class, UserDetailsServiceAutoConfiguration::class])
@SpringBootApplication
open class BankAccountApplication

fun main()
{
    runApplication<BankAccountApplication>()
}