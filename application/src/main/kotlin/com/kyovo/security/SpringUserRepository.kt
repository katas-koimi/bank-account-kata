package com.kyovo.security

import com.kyovo.ports.secondary.UserRepository
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Repository

@Repository
class SpringUserRepository(private val userRepository: UserRepository)
{
    fun getUser(): UserDetails
    {
        val user = userRepository.get()

        return User.builder()
            .username(user.username.value)
            .password(user.password.value)
            .build()
    }
}
