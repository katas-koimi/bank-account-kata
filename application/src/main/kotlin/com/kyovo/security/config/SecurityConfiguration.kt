package com.kyovo.security.config

import com.kyovo.Routes
import com.kyovo.security.JwtAuthenticationFilter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.DefaultSecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
open class SecurityConfiguration(private val authenticationProvider: AuthenticationProvider)
{
    @Bean
    open fun securityFilterChain(http: HttpSecurity,
                                 jwtAuthenticationFilter: JwtAuthenticationFilter): DefaultSecurityFilterChain
    {
        http
            .csrf { it.disable() }
            .authorizeHttpRequests {
                it
                    .requestMatchers(
                        Routes.AUTH,
                        Routes.ERROR,
                        Routes.API_DOC,
                        Routes.SWAGGER_UI,
                        Routes.SWAGGER_V3_API_DOCS
                    )
                    .permitAll()
                    .anyRequest()
                    .fullyAuthenticated()
            }
            .sessionManagement {
                it.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            }
            .authenticationProvider(authenticationProvider)
            .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter::class.java)

        return http.build()
    }
}