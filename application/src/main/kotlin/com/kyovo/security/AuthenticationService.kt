package com.kyovo.security

import com.kyovo.view_models.AuthenticationRequest
import com.kyovo.view_models.AuthenticationResponse
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import java.util.*

@Service
class AuthenticationService(
        private val authManager: AuthenticationManager,
        private val userDetailsService: CustomUserDetailsService,
        private val tokenService: TokenService,
        private val jwtProperties: JwtProperties)
{
    fun authentication(authenticationRequest: AuthenticationRequest): AuthenticationResponse
    {
        authManager.authenticate(
            UsernamePasswordAuthenticationToken(
                authenticationRequest.username,
                authenticationRequest.password
            )
        )
        val user = userDetailsService.loadUserByUsername(authenticationRequest.username)
        val accessToken = createAccessToken(user)

        return AuthenticationResponse(token = accessToken)
    }

    private fun createAccessToken(user: UserDetails) = tokenService.generate(
        userDetails = user,
        expirationDate = getAccessTokenExpiration()
    )

    private fun getAccessTokenExpiration(): Date =
            Date(System.currentTimeMillis() + jwtProperties.accessTokenExpiration)
}