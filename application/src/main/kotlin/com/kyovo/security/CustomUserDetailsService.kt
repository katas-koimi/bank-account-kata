package com.kyovo.security

import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class CustomUserDetailsService(private val userRepository: SpringUserRepository) : UserDetailsService
{
    override fun loadUserByUsername(username: String): UserDetails
    {
        return userRepository.getUser()
    }
}