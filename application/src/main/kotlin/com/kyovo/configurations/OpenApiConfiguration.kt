package com.kyovo.configurations

import com.kyovo.API_VERSION
import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class OpenApiConfiguration
{
    @Bean
    open fun useOpenApi(): OpenAPI = OpenAPI()
        .info(
            Info()
                .title("Bank account API")
                .version(API_VERSION)
        )
        .components(
            Components()
                .addSecuritySchemes(
                    "bearer-key", SecurityScheme()
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("bearer")
                        .bearerFormat("JWT")
                        .description("JWT Token")
                )
        )
        .addSecurityItem(SecurityRequirement().addList("bearer-key"))
}