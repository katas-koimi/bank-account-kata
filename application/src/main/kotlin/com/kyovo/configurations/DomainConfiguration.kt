package com.kyovo.configurations

import com.kyovo.ddd.DomainService
import com.kyovo.ddd.Fake
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(
    basePackages = ["com.kyovo"],
    includeFilters = [ComponentScan.Filter(DomainService::class, Fake::class)]
)
open class DomainConfiguration