package com.kyovo.controllers

import com.kyovo.ApiResponseCode
import com.kyovo.Money
import com.kyovo.Routes
import com.kyovo.exception_handlers.ApiResponseBody
import com.kyovo.exception_handlers.ApiSuccessResponseBody
import com.kyovo.use_cases.DepositMoney
import com.kyovo.use_cases.DisplayBalance
import com.kyovo.use_cases.DisplayTransactionsHistory
import com.kyovo.use_cases.WithdrawMoney
import com.kyovo.view_models.AccountBalanceResource
import com.kyovo.view_models.DepositResource
import com.kyovo.view_models.TransactionResource
import com.kyovo.view_models.WithdrawResource
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@Tag(name = "Account")
@RestController
@RequestMapping(Routes.ACCOUNT)
class BankAccountController(private val depositMoney: DepositMoney,
                            private val displayBalance: DisplayBalance,
                            private val withdrawMoney: WithdrawMoney,
                            private val displayTransactionsHistory: DisplayTransactionsHistory)
{
    @Operation(summary = "Make a deposit")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = ApiResponseCode.OK),
            ApiResponse(responseCode = ApiResponseCode.BAD_REQUEST),
            ApiResponse(responseCode = ApiResponseCode.FORBIDDEN)
        ]
    )
    @PostMapping("/deposit")
    fun deposit(@RequestBody deposit: DepositResource): ResponseEntity<ApiResponseBody>
    {
        depositMoney.execute(Money(deposit.amount))
        return ResponseEntity.ok().body(ApiSuccessResponseBody(HttpStatus.OK.value()))
    }

    @Operation(summary = "Get account balance")
    @ApiResponses(
        value = [ApiResponse(responseCode = ApiResponseCode.FORBIDDEN)]
    )
    @GetMapping("/balance")
    @ResponseStatus(HttpStatus.OK)
    fun getBalance(): ResponseEntity<AccountBalanceResource>
    {
        return ResponseEntity.ok(AccountBalanceResource.from(displayBalance.execute()))
    }

    @Operation(summary = "Make a withdrawal")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = ApiResponseCode.OK),
            ApiResponse(responseCode = ApiResponseCode.BAD_REQUEST),
            ApiResponse(responseCode = ApiResponseCode.FORBIDDEN)
        ]
    )
    @PostMapping("/withdraw")
    fun withdraw(@RequestBody withdraw: WithdrawResource): ResponseEntity<ApiResponseBody>
    {
        withdrawMoney.execute(Money(withdraw.amount))
        return ResponseEntity.ok().body(ApiSuccessResponseBody(HttpStatus.OK.value()))
    }

    @Operation(summary = "List transactions history")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = ApiResponseCode.FORBIDDEN)
        ]
    )
    @GetMapping("/transactions")
    @ResponseStatus(HttpStatus.OK)
    fun getTransactionsHistory(): ResponseEntity<List<TransactionResource>>
    {
        return ResponseEntity.ok(TransactionResource.from(displayTransactionsHistory.execute()))
    }
}