package com.kyovo.controllers

import com.kyovo.ApiResponseCode
import com.kyovo.Routes
import com.kyovo.security.AuthenticationService
import com.kyovo.view_models.AuthenticationRequest
import com.kyovo.view_models.AuthenticationResponse
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Tag(name = "Authentication")
@RestController
@RequestMapping(Routes.AUTH)
class AuthController(private val authenticationService: AuthenticationService)
{
    @Operation(summary = "Authenticate")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = ApiResponseCode.OK),
            ApiResponse(responseCode = ApiResponseCode.FORBIDDEN)
        ]
    )
    @PostMapping
    fun authenticate(@RequestBody authRequest: AuthenticationRequest): AuthenticationResponse =
            authenticationService.authentication(authRequest)
}