package com.kyovo.exception_handlers

import com.kyovo.exceptions.InsufficientBalanceException
import com.kyovo.exceptions.InvalidMoneyAmountException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class CustomExceptionHandler
{
    @ExceptionHandler(
        InsufficientBalanceException::class,
        InvalidMoneyAmountException::class
    )
    fun handleBadRequest(exception: Exception): ResponseEntity<ApiResponseBody> =
            ResponseEntity.badRequest().body(ApiErrorResponseBody(HttpStatus.BAD_REQUEST.value(), exception.message))
}