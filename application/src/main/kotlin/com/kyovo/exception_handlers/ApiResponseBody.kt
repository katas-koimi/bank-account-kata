package com.kyovo.exception_handlers

abstract class ApiResponseBody(open val status: Int, open val message: String?)

data class ApiSuccessResponseBody(override val status: Int) : ApiResponseBody(status, null)

data class ApiErrorResponseBody(override val status: Int, override val message: String?) :
    ApiResponseBody(status, message)
