package com.kyovo.view_models

data class AuthenticationResponse(val token: String)