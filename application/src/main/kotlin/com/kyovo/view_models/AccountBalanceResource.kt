package com.kyovo.view_models

import com.kyovo.Balance

data class AccountBalanceResource(val amount: Double)
{
    companion object
    {
        fun from(balance: Balance): AccountBalanceResource = AccountBalanceResource(balance.amount)
    }
}
