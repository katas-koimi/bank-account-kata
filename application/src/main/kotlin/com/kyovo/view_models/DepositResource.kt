package com.kyovo.view_models

import com.fasterxml.jackson.annotation.JsonProperty

data class DepositResource(@JsonProperty("amount") val amount: Double)
