package com.kyovo.view_models

import com.kyovo.Transaction

data class TransactionResource(val amount: Double, val balance: Double)
{
    companion object
    {
        fun from(domainTransactions: List<Transaction>): List<TransactionResource> =
                domainTransactions.map { TransactionResource(it.amount, it.balance.amount) }
    }
}
