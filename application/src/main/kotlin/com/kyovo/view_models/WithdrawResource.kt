package com.kyovo.view_models

import com.fasterxml.jackson.annotation.JsonProperty

data class WithdrawResource(@JsonProperty("amount") val amount: Double)
