# Bank account kata

From https://github.com/lelionvert/abdessamed-bank-kata

### Language and tools

* Kotlin
* JUnit 5

# Bank Account Kata (EN)

_Une version [française est disponible](#bank-account-kata-fr)_
> This exercice has some flexible implementation.

## The Kata

### MVP - Minimum Valuable Product

#### User Story 1

> As a bank, deposit money from a customer to his account, is allowed when superior to €0.01

#### User Story 2

> As a bank, withdraw money from a customer account, is allowed when no overdraft used

#### User Story 3

> As a bank, a customer can display its account balance

#### User Story 4

> As a bank, a customer can display its account transactions history

### Bonus features

The below features are optional and non-exhaustive.  
There is no priority between them, you can implement the ones you want and even propose yours.

#### REST API

* Suggest a REST API using http to operate services realized in the MVP
* Secure your API
* Use a non-blocking solution

#### Customers & Accounts

* The bank has multiple customers
* A customer can have multiple accounts

#### Persistence

* Suggest a data persistence solution

#### User Interface

* Suggest a UI to operate services realized in the MVP

#### CI/CD

* Use Gradle instead of Maven
* Suggest a CI/CD system for the project
* Suggest End to End tests for your artifact

----------

# Bank Account Kata (FR)

> Ceci est un exercice assez libre d'implémentation.

## Le Kata

### Minimum Valuable Product - MVP

#### User Story 1

> En tant que banque, j'accepte le dépôt d'argent d'un client vers son compte, s'il est supérieur à 0,01€

#### User Story 2

> En tant que banque, j'accepte le retrait d'argent d'un client depuis son compte, s'il n'utilise pas le découvert

#### User Story 3

> En tant que banque, j'offre la possibilité à mon client de consulter le solde de son compte

#### User Story 4

> En tant que banque, j'offre la possibilité à mon client de consulter l'historique des transactions sur son compte

### Features bonus

Les fonctionnalités suivantes sont optionnelles et non exhaustives.  
Elles n'ont pas de priorité entre elles, vous pouvez implémenter celles qui vous intéressent ou même en proposer
d'autres.

#### API REST

* Proposer une API REST consommable via http pour interagir avec les services réalisé dans le MVP
* Sécuriser l'API
* Utiliser une solution non-bloquante

#### Clients & Comptes

* La banque a plusieurs clients
* Un client peut avoir plusieurs comptes

#### Persistence

* Proposer une solution de persistence des données

#### Interface Utilisateur

* Proposer une interface graphique pour interagir avec les services réalisés dans le MVP

#### CI/CD

* Utiliser Gradle au lieu de Maven
* Proposer un system de CI/CD pour le projet
* Proposer des tests End to End à destination de votre livrable
