package com.kyovo

import com.kyovo.exceptions.InsufficientBalanceException
import com.kyovo.ports.primary.AccountService
import com.kyovo.ports.secondary.fakes.FakeAccountService
import com.kyovo.ports.secondary.fakes.InMemoryTransactionRepository
import com.kyovo.use_cases.DepositMoney
import com.kyovo.use_cases.WithdrawMoney
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

class WithdrawMoneyTest
{
    companion object
    {
        @JvmStatic
        fun nominalWithdrawalsOperations() = listOf<Arguments>(
            Arguments.of(200.0, 50.0, 150.0),
            Arguments.of(2485.75, 2485.75, 0.0)
        )

        @JvmStatic
        fun errorWithdrawalsOperations() = listOf<Arguments>(
            Arguments.of(1500.0, 1500.5),
            Arguments.of(8794.35, 10000.0)
        )
    }

    private val accountService: AccountService = FakeAccountService(InMemoryTransactionRepository())

    @ParameterizedTest
    @MethodSource("nominalWithdrawalsOperations")
    fun `given account with certain amount when withdraw less or equal than balance then success`(
            initialBalance: Double,
            withdrawalAmount: Double,
            finalBalance: Double)
    {
        given(initialBalance)

        val withdrawMoney = WithdrawMoney(accountService)
        withdrawMoney.execute(Money(withdrawalAmount))

        assertThat(accountService.balance()).isEqualTo(Balance(finalBalance))
    }

    @ParameterizedTest
    @MethodSource("errorWithdrawalsOperations")
    fun `given account with certain amount when withdraw more than balance then error`(initialBalance: Double,
                                                                                       withdrawalsAmount: Double)
    {
        given(initialBalance)

        val exception = assertThrows<InsufficientBalanceException> {
            WithdrawMoney(accountService).execute(Money(withdrawalsAmount))
        }

        assertAll(
            { assertThat(exception.message).isEqualTo("Insufficient balance for withdrawal") },
            { assertThat(accountService.balance()).isEqualTo(Balance(initialBalance)) }
        )
    }

    private fun given(initialBalance: Double)
    {
        val depositMoney = DepositMoney(accountService)
        depositMoney.execute(Money(initialBalance))
    }
}