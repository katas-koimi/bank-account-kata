package com.kyovo

import com.kyovo.exceptions.InvalidMoneyAmountException
import com.kyovo.ports.primary.AccountService
import com.kyovo.ports.secondary.fakes.FakeAccountService
import com.kyovo.ports.secondary.fakes.InMemoryTransactionRepository
import com.kyovo.use_cases.DepositMoney
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class DepositMoneyTest
{
    private val accountService: AccountService = FakeAccountService(InMemoryTransactionRepository())
    private val depositMoney = DepositMoney(accountService)

    @ParameterizedTest
    @ValueSource(doubles = [1000.0, 200.0])
    fun `given some amount when deposit then account balance is that amount`(amount: Double)
    {
        depositMoney.execute(Money(amount))

        assertThat(accountService.balance()).isEqualTo(Balance(amount))
    }

    @Test
    fun `when make multiple deposits then account balance is updated`()
    {
        depositMoney.execute(Money(200.0))
        depositMoney.execute(Money(324.5))
        depositMoney.execute(Money(100.0))

        assertThat(accountService.balance()).isEqualTo(Balance(624.5))
    }

    @Test
    fun `given deposit amount of 0,01 then error`()
    {
        val exception = assertThrows<InvalidMoneyAmountException> {
            depositMoney.execute(Money(0.01))
        }

        assertAll(
            { assertThat(exception.message).isEqualTo("The deposit amount must be more than 0.01") },
            { assertThat(accountService.balance()).isEqualTo(Balance(0.0)) }
        )
    }
}