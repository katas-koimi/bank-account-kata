package com.kyovo

import com.kyovo.ports.primary.AccountService
import com.kyovo.ports.secondary.fakes.FakeAccountService
import com.kyovo.ports.secondary.fakes.InMemoryTransactionRepository
import com.kyovo.use_cases.DepositMoney
import com.kyovo.use_cases.DisplayBalance
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class DisplayBalanceTest
{
    private val accountService: AccountService = FakeAccountService(InMemoryTransactionRepository())

    @Test
    fun `given empty account then display account balance is 0`()
    {
        val displayBalance = DisplayBalance(accountService)

        assertThat(displayBalance.execute()).isEqualTo(Balance(0.0))
    }

    @ParameterizedTest
    @ValueSource(doubles = [4623.55, 78_250_978.0])
    fun `given account with certain balance then display that account balance`(amount: Double)
    {
        val displayBalance = DisplayBalance(accountService)

        DepositMoney(accountService).execute(Money(amount))

        assertThat(displayBalance.execute()).isEqualTo(Balance(amount))
    }
}