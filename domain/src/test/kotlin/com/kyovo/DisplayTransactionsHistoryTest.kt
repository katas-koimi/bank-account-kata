package com.kyovo

import com.kyovo.ports.primary.AccountService
import com.kyovo.ports.secondary.fakes.FakeAccountService
import com.kyovo.ports.secondary.fakes.InMemoryTransactionRepository
import com.kyovo.use_cases.DepositMoney
import com.kyovo.use_cases.DisplayTransactionsHistory
import com.kyovo.use_cases.WithdrawMoney
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class DisplayTransactionsHistoryTest
{
    private val accountService: AccountService = FakeAccountService(InMemoryTransactionRepository())

    @Test
    fun `given no transactions then history is empty`()
    {
        assertThat(DisplayTransactionsHistory(accountService).execute()).isEmpty()
    }

    @ParameterizedTest
    @ValueSource(doubles = [2500.45, 300.0])
    fun `given one deposit transaction then display it`(amount: Double)
    {
        val depositMoney = DepositMoney(accountService)
        depositMoney.execute(Money(amount))

        val transactionsHistory = DisplayTransactionsHistory(accountService).execute()

        assertThat(transactionsHistory).isEqualTo(
            listOf(
                Transaction(amount, Balance(amount))
            )
        )
    }

    @Test
    fun `given multiple deposit transactions then display all showing last deposit first`()
    {
        val depositMoney = DepositMoney(accountService)
        depositMoney.execute(Money(800.0))
        depositMoney.execute(Money(1200.0))
        depositMoney.execute(Money(570.65))

        val transactionsHistory = DisplayTransactionsHistory(accountService).execute()

        assertThat(transactionsHistory).isEqualTo(
            listOf(
                Transaction(570.65, Balance(2570.65)),
                Transaction(1200.0, Balance(2000.0)),
                Transaction(800.0, Balance(800.0))
            )
        )
    }

    @Test
    fun `given all kind of transactions then display all`()
    {
        val depositMoney = DepositMoney(accountService)
        val withdrawMoney = WithdrawMoney(accountService)
        depositMoney.execute(Money(3000.0))
        withdrawMoney.execute(Money(250.0))
        withdrawMoney.execute(Money(30.0))
        depositMoney.execute(Money(15750.0))
        withdrawMoney.execute(Money(2500.0))

        val transactionsHistory = DisplayTransactionsHistory(accountService).execute()

        assertThat(transactionsHistory).isEqualTo(
            listOf(
                Transaction(-2500.0, Balance(15970.0)),
                Transaction(15750.0, Balance(18470.0)),
                Transaction(-30.0, Balance(2720.0)),
                Transaction(-250.0, Balance(2750.0)),
                Transaction(3000.0, Balance(3000.0))
            )
        )
    }
}