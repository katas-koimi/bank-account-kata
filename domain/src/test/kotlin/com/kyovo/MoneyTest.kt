package com.kyovo

import com.kyovo.exceptions.InvalidMoneyAmountException
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class MoneyTest
{
    @ParameterizedTest
    @ValueSource(doubles = [-1.0, -0.75, 0.0])
    fun `money must be worth at least 0,01`(amount: Double)
    {
        val exception = assertThrows<InvalidMoneyAmountException> {
            Money(amount)
        }

        Assertions.assertThat(exception.message).isEqualTo("Money must be worth at least 0.01")
    }
}