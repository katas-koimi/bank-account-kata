package com.kyovo

import com.kyovo.exceptions.InvalidMoneyAmountException

@JvmInline
value class Money(val value: Double)
{
    companion object
    {
        const val MINIMUM_VALID_MONEY_AMOUNT = 0.01
        const val MINIMUM_DEPOSIT_AMOUNT = 0.01
    }

    init
    {
        require(valueIsTheMinimumRequired()) { throw InvalidMoneyAmountException() }
    }

    private fun valueIsTheMinimumRequired() = value >= MINIMUM_VALID_MONEY_AMOUNT

    operator fun compareTo(amount: Double): Int = this.value.compareTo(amount)

    fun isTheMinimumRequiredForDeposit(): Boolean = value > MINIMUM_DEPOSIT_AMOUNT
}