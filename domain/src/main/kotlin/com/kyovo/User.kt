package com.kyovo

data class User(val username: Username, val password: Password)

@JvmInline
value class Username(val value: String)

@JvmInline
value class Password(val value: String)