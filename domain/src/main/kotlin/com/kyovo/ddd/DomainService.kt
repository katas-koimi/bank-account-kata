package com.kyovo.ddd

/**
 *
 *
 * A Domain Service, i.e. a feature that belongs to the domain
 *
 *
 * @see [Domain-Driven Design Reference](https://www.domainlanguage.com/wp-content/uploads/2016/05/DDD_Reference_2015-03.pdf)
 */
@Retention(AnnotationRetention.RUNTIME)
annotation class DomainService
