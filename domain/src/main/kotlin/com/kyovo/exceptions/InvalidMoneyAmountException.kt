package com.kyovo.exceptions

import com.kyovo.Money.Companion.MINIMUM_VALID_MONEY_AMOUNT

class InvalidMoneyAmountException(
        override val message: String? = "Money must be worth at least $MINIMUM_VALID_MONEY_AMOUNT"
) : Exception(message)
