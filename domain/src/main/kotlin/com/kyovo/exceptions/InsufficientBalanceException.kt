package com.kyovo.exceptions

class InsufficientBalanceException : Exception("Insufficient balance for withdrawal")
