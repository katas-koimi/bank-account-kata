package com.kyovo.ports.secondary.fakes

import com.kyovo.Transaction
import com.kyovo.ddd.Fake
import com.kyovo.ports.secondary.TransactionRepository

@Fake
class InMemoryTransactionRepository : TransactionRepository
{
    private val transactions = mutableListOf<Transaction>()

    override fun list(): List<Transaction> = transactions.reversed()

    override fun save(transaction: Transaction)
    {
        transactions.add(transaction)
    }
}
