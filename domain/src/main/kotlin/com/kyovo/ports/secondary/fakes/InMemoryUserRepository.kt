package com.kyovo.ports.secondary.fakes

import com.kyovo.Password
import com.kyovo.User
import com.kyovo.Username
import com.kyovo.ddd.Fake
import com.kyovo.ports.secondary.UserRepository

@Fake
class InMemoryUserRepository : UserRepository
{
    //    PASSWORD = $2a$10$5BQZ/kL.G27y8AZGjk0E6e0cOQZMsZzWXq07pW3NHqdMoFhCdzWKO
    //    PASSWORD = $2a$10$19PXE3h1.SPyDdBitqWMhuqzqr//gtfjpZW7Cmqa1IjFblIprk/uK
    // CLEAR = alanp@ss
    override fun get(): User
    {
        val password = """${'$'}2a${'$'}10${'$'}19PXE3h1.SPyDdBitqWMhuqzqr//gtfjpZW7Cmqa1IjFblIprk/uK"""
        return User(Username("alan"), Password(password))
    }
}