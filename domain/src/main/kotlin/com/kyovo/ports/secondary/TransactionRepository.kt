package com.kyovo.ports.secondary

import com.kyovo.Transaction

interface TransactionRepository
{
    fun list(): List<Transaction>
    fun save(transaction: Transaction)
}
