package com.kyovo.ports.secondary.fakes

import com.kyovo.Balance
import com.kyovo.Money
import com.kyovo.Transaction
import com.kyovo.ddd.Fake
import com.kyovo.ports.primary.AccountService
import com.kyovo.ports.secondary.TransactionRepository

@Fake
class FakeAccountService(private val transactionRepository: TransactionRepository) : AccountService
{
    private var balance: Double = 0.0

    override fun deposit(amount: Money)
    {
        this.balance += amount.value
        transactionRepository.save(Transaction(amount.value, Balance(balance)))
    }

    override fun balance(): Balance = Balance(balance)

    override fun withdraw(amount: Money)
    {
        balance -= amount.value
        transactionRepository.save(Transaction(-amount.value, Balance(balance)))
    }

    override fun listTransactionsHistory(): List<Transaction> =
            transactionRepository.list()
}
