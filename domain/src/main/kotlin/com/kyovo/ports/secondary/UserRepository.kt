package com.kyovo.ports.secondary

import com.kyovo.User

interface UserRepository
{
    fun get(): User
}