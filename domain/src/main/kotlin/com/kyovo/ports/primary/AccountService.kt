package com.kyovo.ports.primary

import com.kyovo.Balance
import com.kyovo.Money
import com.kyovo.Transaction

interface AccountService
{
    fun deposit(amount: Money)
    fun balance(): Balance
    fun withdraw(amount: Money)
    fun listTransactionsHistory(): List<Transaction>
}