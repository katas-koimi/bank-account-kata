package com.kyovo

data class Transaction(val amount: Double, val balance: Balance)
