package com.kyovo.use_cases

import com.kyovo.Money
import com.kyovo.ddd.DomainService
import com.kyovo.exceptions.InsufficientBalanceException
import com.kyovo.ports.primary.AccountService

@DomainService
class WithdrawMoney(private val accountService: AccountService)
{
    @Throws(InsufficientBalanceException::class)
    fun execute(amount: Money)
    {
        if (insufficientBalanceToWithdraw(amount))
            throw InsufficientBalanceException()
        else
            accountService.withdraw(amount)
    }

    private fun insufficientBalanceToWithdraw(amount: Money) = amount.value > accountService.balance().amount
}
