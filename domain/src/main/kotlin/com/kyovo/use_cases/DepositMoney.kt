package com.kyovo.use_cases

import com.kyovo.Money
import com.kyovo.ddd.DomainService
import com.kyovo.exceptions.InvalidMoneyAmountException
import com.kyovo.ports.primary.AccountService

@DomainService
class DepositMoney(private val accountService: AccountService)
{
    @Throws(InvalidMoneyAmountException::class)
    fun execute(amount: Money)
    {
        if (amount.isTheMinimumRequiredForDeposit())
        {
            accountService.deposit(amount)
        }
        else
        {
            throw InvalidMoneyAmountException("The deposit amount must be more than 0.01")
        }
    }
}
