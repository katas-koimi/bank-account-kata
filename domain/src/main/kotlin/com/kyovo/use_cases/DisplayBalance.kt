package com.kyovo.use_cases

import com.kyovo.Balance
import com.kyovo.ddd.DomainService
import com.kyovo.ports.primary.AccountService

@DomainService
class DisplayBalance(private val accountService: AccountService)
{
    fun execute(): Balance = accountService.balance()
}
