package com.kyovo.use_cases

import com.kyovo.Transaction
import com.kyovo.ddd.DomainService
import com.kyovo.ports.primary.AccountService

@DomainService
class DisplayTransactionsHistory(private val accountService: AccountService)
{
    fun execute(): List<Transaction> = accountService.listTransactionsHistory()
}
