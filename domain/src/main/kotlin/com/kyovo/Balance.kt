package com.kyovo

@JvmInline
value class Balance(val amount: Double)